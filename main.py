from core.game_mechanics import initialize_game, run_game
from core.user_interface import UserInterface

from utils.load_config import load_config


def main():
    config = load_config()

    # Initialize the game
    player, list_space_invaders = initialize_game(config)

    user_interface = UserInterface(screen_width=config['screen']['width'],
                                   screen_height=config['screen']['height'])

    run_game(player=player,
             list_space_invaders=list_space_invaders,
             user_interface=user_interface,
             bullet_settings=config['bullet_settings'])


if __name__ == '__main__':
    main()
