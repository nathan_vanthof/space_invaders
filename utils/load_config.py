import yaml


def load_config(path='config/config.yaml'):
    config = yaml.load(open(path), Loader=yaml.FullLoader)
    return config
