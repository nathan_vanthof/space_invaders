import unittest
from time import sleep

from core.player import Player


class TestPlayer(unittest.TestCase):

    def setUp(self):
        self.player = Player(height=10,
                             width=20,
                             speed=10,
                             time_between_shooting=2,
                             x_coordinate=30,
                             y_coordinate=100)

    def test_update_position(self):
        """
        Test if the position is updated correctly,
        first go all the way to the right
        then go all the way to the left
        then move back once to the right
        and stay there
        """

        positions = [40, 50, 60, 70, 80, 80, 80]
        for position in positions:
            self.player.update_position(direction=1,
                                        screen_width=100)
            self.assertEqual(position, self.player.x_coordinate, msg='Move to the right')

        positions = [70, 60, 50, 40, 30, 20, 10, 0, 0, 0]
        for position in positions:
            self.player.update_position(direction=-1,
                                        screen_width=100)
            self.assertEqual(position, self.player.x_coordinate, msg='Move to the left')

        self.player.update_position(direction=1,
                                    screen_width=100)
        positions = [10, 10, 10]
        for position in positions:
            self.player.update_position(direction=0,
                                        screen_width=100)
            self.assertEqual(position, self.player.x_coordinate, msg='Stay in place')

    def test_shoot_bullet(self):
        """
        Shoot 4 bullets:
        1 and 2 directly follow
        1 second sleep
        bullet 3
        1.5 seconds sleep
        bullet 4

        Only bullet 1 and 4 should be shot
        """

        bullet_1 = self.player.shoot_bullet(bullet_settings={'speed_y': 10, 'width': 5, 'height': 10, 'damage': 1})
        bullet_2 = self.player.shoot_bullet(bullet_settings={'speed_y': 10, 'width': 5, 'height': 10, 'damage': 1})
        sleep(1)
        bullet_3 = self.player.shoot_bullet(bullet_settings={'speed_y': 10, 'width': 5, 'height': 10, 'damage': 1})
        sleep(1.5)
        bullet_4 = self.player.shoot_bullet(bullet_settings={'speed_y': 10, 'width': 5, 'height': 10, 'damage': 1})

        self.assertIsNotNone(bullet_1, msg='bullet 1')
        self.assertIsNone(bullet_2, msg='~ 0 seconds since bullet 1')
        self.assertIsNone(bullet_3, msg='~ 1.5 seconds since bullet 1')
        self.assertIsNotNone(bullet_4, msg='~ 2.5 seconds since bullet 1')


if __name__ == '__main__':
    unittest.main()
