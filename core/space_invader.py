import pygame

from core.entity import Entity


class SpaceInvader(Entity):

    def __init__(self, health, x_coordinate, y_coordinate, maximum_x_coordinate, maximum_y_coordinate, speed_x, y_incrementation, width, height):
        """
        An enemy object, which can move based on x_coordinate, y_coordinate and speed.
        If health drops below 0, it dies

        :param int health: enemy health, should be above, otherwise it dies
        :param int x_coordinate: the x-position on screen
        :param int y_coordinate: the y-position on screen
        :param int maximum_x_coordinate: the position at x where the space invader will turn
        :param int maximum_y_coordinate: the position at y where the space invader reached the bottom
        :param int speed_x: the movement speed of the enemy
        :param int y_incrementation: the jump in y-position after each lap
        :param int width: width of the space invader
        :param int height: height of the space invader
        """

        Entity.__init__(self, width=width, height=height, x_coordinate=x_coordinate, y_coordinate=y_coordinate)
        self.health = health
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate
        self.maximum_x_coordinate = maximum_x_coordinate
        self.maximum_y_coordinate = maximum_y_coordinate
        self.speed = speed_x
        self.width = width
        self.height = height
        self.y_incrementation = y_incrementation

        self.is_alive = True
        # 1 is to the right, -1 is to the left
        self.direction = 1

    def update_position(self):
        """
        Change the x-position with the speed value in the direction of movement. If the space invader gets to the edge
        it changes direction and moves down.
        """

        if not self.is_alive:
            return

        self._update_coordinates()
        self._update_rectangle()

    def _update_coordinates(self):
        if self.direction == 1:
            if self.x_coordinate + self.speed > self.maximum_x_coordinate - self.width:
                self.direction = -1
                self.y_coordinate += self.y_incrementation
            else:
                self.x_coordinate += self.direction * self.speed
        else:
            if self.x_coordinate - self.speed < 0:
                self.direction = 1
                self.y_coordinate += self.y_incrementation
            else:
                self.x_coordinate += self.direction * self.speed

    def get_hit(self, damage):
        """
        Decrease the health of the space invader by damage, if health goes below 0, the space invader dies

        :param int damage: the amount of damage done to the space invader
        """

        self.health -= damage
        self.is_alive = self.health > 0

    def reached_bottom(self):
        return self.y_coordinate >= self.maximum_y_coordinate
