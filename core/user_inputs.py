import pygame
from pygame.locals import *


def check_player_quits():
    for event in pygame.event.get():
        if event.type == QUIT:
            return False

    key = pygame.key.get_pressed()
    if key[K_ESCAPE]:
        return False

    return True


def get_user_movement():
    key = pygame.key.get_pressed()

    if key[K_LEFT]:
        direction = -1
    elif key[K_RIGHT]:
        direction = 1
    else:
        direction = 0

    return direction


def get_bullet_shot():
    key = pygame.key.get_pressed()

    if key[K_SPACE]:
        return True
    return False