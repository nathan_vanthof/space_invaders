import numpy as np
import pygame
import time

from core.space_invader import SpaceInvader
from core.player import Player
from core.user_inputs import check_player_quits, get_user_movement, get_bullet_shot


def initialize_game(config):
    list_space_invaders = []
    for x_coordinate in np.linspace(0, config['screen']['width'], config['space_invaders']['number_of_columns'],
                                    endpoint=False):
        for y_coordinate in np.linspace(0, config['screen']['height'] // 2, config['space_invaders']['number_of_rows']):
            space_invader = SpaceInvader(health=config['space_invader']['health'],
                                         x_coordinate=x_coordinate,
                                         y_coordinate=y_coordinate,
                                         speed_x=config['space_invader']['speed_x'],
                                         y_incrementation=config['space_invader']['y_incrementation'],
                                         width=config['space_invader']['width'],
                                         height=config['space_invader']['height'],
                                         maximum_x_coordinate=config['space_invader']['maximum_x_coordinate'],
                                         maximum_y_coordinate=config['space_invader']['maximum_y_coordinate'])
            list_space_invaders.append(space_invader)

    player = Player(x_coordinate=25,
                    y_coordinate=config['screen']['height'] - config['player']['height'],
                    speed=config['player']['speed'],
                    width=config['player']['width'],
                    height=config['player']['height'],
                    time_between_shooting=config['player']['time_between_shooting'])

    return player, list_space_invaders


def check_game_running(list_space_invaders):
    """
    Check if either the space invaders have reached the bottom, or if they are all killed

    :param list_space_invaders: list of all the space invaders
    :return: True if the game is still running, else False
    """

    if len(list_space_invaders) == 0:
        return False
    if any([space_invader.reached_bottom() for space_invader in list_space_invaders]):
        return False
    return True


def check_bullet_space_invader_interaction(list_bullets, list_space_invaders):
    """
    Check which bullets and space invaders overlap, remove any which do

    :param list list_bullets: list of all bullets currently active
    :param list list_space_invaders: list of all space invaders that are alive
    :return: list_bullets, list_space_invaders without any bullets or space invaders touching
    """

    bullets_to_delete = []
    space_invaders_to_delete = []

    for bullet in list_bullets:
        for space_invader in list_space_invaders:
            if bullet.rectangle.colliderect(space_invader.rectangle):
                bullets_to_delete.append(bullet)
                space_invader.get_hit(damage=bullet.damage)
                if not space_invader.is_alive:
                    space_invaders_to_delete.append(space_invader)

    list_bullets = [i for i in list_bullets if i not in bullets_to_delete]
    list_space_invaders = [i for i in list_space_invaders if i not in space_invaders_to_delete]

    return list_bullets, list_space_invaders


def run_game(player, list_space_invaders, user_interface, bullet_settings):
    pygame.init()

    game_running = True
    list_bullets = []

    while game_running:
        # Check if the player wants to leave
        game_running = check_player_quits()
        # Check if the game has been won/lost
        if game_running:
            game_running = check_game_running(list_space_invaders)

        # Get user inputs
        direction = get_user_movement()
        bullet_shot = get_bullet_shot()

        # Execute effects caused by player
        if bullet_shot:
            bullet = player.shoot_bullet(bullet_settings=bullet_settings)
            if bullet is not None:
                list_bullets.append(bullet)

        player.update_position(direction=direction,
                               screen_width=user_interface.screen_width)

        # Update all 'NPC' entities
        for space_invader in list_space_invaders:
            space_invader.update_position()
        for bullet in list_bullets:
            bullet.update_position()

        list_bullets, list_space_invaders = check_bullet_space_invader_interaction(list_bullets, list_space_invaders)
        list_bullets = [bullet for bullet in list_bullets if not bullet.out_of_screen()]

        # Draw everything
        user_interface.draw_screen(player=player,
                                   list_space_invaders=list_space_invaders,
                                   list_bullets=list_bullets)

        # Sleep to get a 'standard' time interval
        time.sleep(1/23.)

    pygame.quit()


