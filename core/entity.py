import pygame


class Entity:

    def __init__(self, width, height, x_coordinate, y_coordinate):
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate
        self.rectangle = pygame.Rect(x_coordinate, y_coordinate, width, height)

    def _update_rectangle(self):
        self.rectangle.x = self.x_coordinate
        self.rectangle.y = self.y_coordinate
